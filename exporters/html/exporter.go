package html

import (
	"OpenHLADocsGen/data"
	"OpenHLADocsGen/diagrams"
	_ "OpenHLADocsGen/exporters/html/statik"
	"github.com/gomarkdown/markdown"
	"github.com/iancoleman/strcase"
	"github.com/rakyll/statik/fs"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

type Exporter struct {
}

type GenerateImageFunction func(diagramName string, hla data.OpenHLA) ([]byte, error)

func (e Exporter) Export(spec data.OpenHLA, outputDir string) error {
	if err := os.RemoveAll(outputDir); err != nil {
		return err
	}
	if err := GenerateFromTemplate(outputDir, "index", spec); err != nil {
		return err
	}
	if err := CopyFromStatik(outputDir, "css/style.css"); err != nil {
		return err
	}
	if err := CopyFromStatik(outputDir, "css/bootstrap.min.css"); err != nil {
		return err
	}
	if err := CopyFromStatik(outputDir, "js/bootstrap.bundle.min.js"); err != nil {
		return err
	}
	if err := GenerateDiagram("integrations", outputDir, spec, diagrams.BuildIntegrationsDiagram); err != nil {
		return err
	}

	return GeneratePlatformDiagram(outputDir, spec)
}

func GeneratePlatformDiagram(outputDir string, spec data.OpenHLA) error {
	diagramName := "platform_diagram"
	return GenerateDiagram(diagramName, outputDir, spec, func(diagramName string, hla data.OpenHLA) ([]byte, error) {
		return diagrams.GetDiagramImage(diagramName, spec.Info.Name, diagrams.BuildPlatformDiagram(spec))
	})
}

func GenerateDiagram(diagramName string, outputDir string, spec data.OpenHLA, generateImageFunction GenerateImageFunction) error {
	content, err := generateImageFunction(diagramName, spec)
	if err != nil {
		return err
	}

	return SaveToOutput(filepath.Join(outputDir, "images"), diagramName+".png", content)
}

func md(md string) template.HTML {
	return template.HTML(markdown.ToHTML([]byte(md), nil, nil))
}

func GenerateFromTemplate(outputDir string, templateName string, data data.OpenHLA) error {
	contents, err := GetFromStatik(templateName + ".gohtml")
	if err != nil {
		return err
	}

	err = EnsureDir(outputDir, "index.gohtml")
	if err != nil {
		return err
	}

	outputIndexFilePath := filepath.Join(outputDir, templateName+".html")
	f, err := os.Create(outputIndexFilePath)
	if err != nil {
		return err
	}
	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(f)

	t := template.New("tmpl")
	t.Funcs(template.FuncMap{
		"md":    md,
		"getId": strcase.ToLowerCamel,
	})
	t, _ = t.Parse(string(contents))
	return t.Execute(f, data)
}

func CopyFromStatik(outputDir string, file string) error {
	contents, err := GetFromStatik(file)
	if err != nil {
		return err
	}

	return SaveToOutput(outputDir, file, contents)
}

func SaveToOutput(outputDir string, file string, contents []byte) error {
	if err := EnsureDir(outputDir, file); err != nil {
		return err
	}

	fc, err := os.Create(filepath.Join(outputDir, file))
	if err != nil {
		return err
	}
	defer func(fc *os.File) {
		err := fc.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(fc)

	_, err = fc.Write(contents)
	if err != nil {
		return err
	}

	return nil
}

func GetFromStatik(file string) ([]byte, error) {
	statikFS, err := fs.New()
	if err != nil {
		return nil, err
	}

	r, err := statikFS.Open(filepath.Join("/", file))
	if err != nil {
		return nil, err
	}
	defer func(r http.File) {
		err := r.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(r)

	contents, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	return contents, nil
}

func EnsureDir(outputDir string, file string) error {
	path := filepath.Join(outputDir, file)
	err := os.MkdirAll(filepath.Dir(path), os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}
