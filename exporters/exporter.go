package exporters

import (
	"OpenHLADocsGen/data"
	"OpenHLADocsGen/exporters/html"
	"fmt"
)

type Exporter interface {
	Export(spec data.OpenHLA, output string) error
}

var exporters = make(map[string]Exporter)

func Register(name string, exporter Exporter) error {
	exporters[name] = exporter
	return nil
}

func Get(name string) (Exporter, error) {
	if exporter, ok := exporters[name]; ok {
		if ok {
			return exporter, nil
		}
	}
	return nil, fmt.Errorf("exporter %s doesn't exists", name)
}

func List() []string {
	keys := make([]string, len(exporters))

	i := 0
	for exporterName := range exporters {
		keys[i] = exporterName
		i++
	}

	return keys
}

func RegisterBase() error {
	err := Register("html", html.Exporter{})
	if err != nil {
		return err
	}

	return nil
}
