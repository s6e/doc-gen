## OpenHLA docs generator

Tool to genarate documentation based on [OpenHLA spec](https://bitbucket.org/s6e/spec/).

### Usage

```bash
usage: print [-h|--help] [-s|--spec "<value>"] [-d|--output-dir "<value>"]
             [-o|--output "<value>"]

             Prints provided string to stdout

Arguments:

  -h  --help        Print help information
  -s  --spec        OpenHLA specification file. Default: openhla.json
  -d  --output-dir  Output directory. Default: build
  -o  --output      Output document format. Allowed values: html. Default: html
```

### Build

```bash
go build -o open-hla-docs-gen
```

For specification examples see **examples** folder
