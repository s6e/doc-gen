package diagrams

import (
	"OpenHLADocsGen/data"
	"github.com/blushft/go-diagrams/diagram"
)

func BuildPlatformDiagram(hla data.OpenHLA) DiagramBuilderFunc {
	layers := make(map[string]*diagram.Group)
	cmpts := make(map[string]*diagram.Node)
	for _, c := range hla.Components {
		if c.Layer != "" {
			if layers[c.Layer] == nil {
				layers[c.Layer] = diagram.NewGroup(c.Layer).Label(c.Layer)
			}
		}

		cmpts[c.Name] = GetNodeByType(c.Type, diagram.NodeLabel(c.Name))
	}

	return func(d *diagram.Diagram) {
		for _, c := range hla.Components {
			if c.Layer != "" {
				layers[c.Layer].Add(cmpts[c.Name])
			} else {
				d.Add(cmpts[c.Name])
			}

			for _, dep := range c.DependsOn {
				d.Connect(cmpts[c.Name], cmpts[dep], diagram.Forward())
			}
		}

		for _, layer := range layers {
			d.Group(layer)
		}
	}
}
