package diagrams

import (
	"github.com/blushft/go-diagrams/diagram"
	"github.com/blushft/go-diagrams/nodes/gcp"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

type DiagramBuilderFunc func(*diagram.Diagram)

func DotToImage(diagramName string, dotString []byte) ([]byte, error) {
	dir, err := ioutil.TempDir("", "docsGen")
	if err != nil {
		return nil, err
	}
	defer func(path string) {
		err := os.RemoveAll(path)
		if err != nil {
			log.Fatal(err)
		}
	}(dir)

	workingDir, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	err = os.Chdir(dir)
	if err != nil {
		return nil, err
	}
	defer func(dir string) {
		err := os.Chdir(dir)
		if err != nil {
			log.Fatal(err)
		}
	}(workingDir)

	f, err := os.Create(filepath.Join(dir, diagramName+".dot"))
	if err != nil {
		return nil, err
	}
	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(f)

	_, err = f.Write(dotString)
	if err != nil {
		return nil, err
	}

	output, err := exec.Command("dot", "-Tpng", diagramName+".dot").Output()
	if err != nil {
		return nil, err
	}

	return output, nil
}

func GetDiagramImage(diagramName string, diagramLabel string, prepareDiagram DiagramBuilderFunc) ([]byte, error) {
	dir, err := ioutil.TempDir("", "docsGen")
	if err != nil {
		return nil, err
	}
	defer func(path string) {
		err := os.RemoveAll(path)
		if err != nil {
			log.Fatal(err)
		}
	}(dir)

	workingDir, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	err = os.Chdir(dir)
	if err != nil {
		return nil, err
	}
	defer func(dir string) {
		err := os.Chdir(dir)
		if err != nil {
			log.Fatal(err)
		}
	}(workingDir)

	d, err := diagram.New(
		func(options *diagram.Options) {
			options.Name = diagramName
		},
		diagram.Label(diagramLabel),
		diagram.Filename(diagramName),
	)
	if err != nil {
		return nil, err
	}

	prepareDiagram(d)
	if err = d.Render(); err != nil {
		return nil, err
	}

	err = os.Chdir(filepath.Join(dir, diagramName))
	if err != nil {
		return nil, err
	}

	output, err := exec.Command("dot", "-Tpng", diagramName+".dot").Output()
	if err != nil {
		return nil, err
	}

	return output, nil
}

func GetNodeByType(t string, options ...diagram.NodeOption) *diagram.Node {
	switch t {
	case "service":
		return gcp.Api.Endpoints(options...)
	case "load-balancer":
		return gcp.Network.LoadBalancing(options...)
	case "lb":
		return gcp.Network.LoadBalancing(options...)
	case "database":
		return gcp.Database.Sql(options...)
	case "db":
		return gcp.Database.Sql(options...)
	case "cache":
		return gcp.Database.Memorystore(options...)
	case "worker":
		return gcp.Compute.ComputeEngine(options...)
	case "dns":
		return gcp.Network.Dns(options...)
	default:
		return gcp.Compute.ComputeEngine(options...)
	}
}
