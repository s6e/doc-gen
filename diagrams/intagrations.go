package diagrams

import (
	"OpenHLADocsGen/data"
	"bytes"
	"fmt"
	"github.com/goccy/go-graphviz"
	"github.com/goccy/go-graphviz/cgraph"
	"log"
	"strings"
)

func BuildIntegrationsDiagram(diagramName string, hla data.OpenHLA) ([]byte, error) {
	g := graphviz.New()
	graph, err := g.Graph()
	graph.SetRankDir(cgraph.LRRank)

	if err != nil {
		return nil, err
	}
	defer func() {
		err := graph.Close()
		if err != nil {
			log.Fatal(err)
		}
		err = g.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	platformNode, err := graph.CreateNode(hla.Info.Name)
	if err != nil {
		return nil, err
	}

	platformNode.SetShape(cgraph.HexagonShape)

	nodes := make(map[string]*cgraph.Node)
	integrations := make(map[string]map[data.Direction][]string)

	for _, c := range hla.Components {
		for _, i := range c.Integrations {
			u := make([]string, len(i.Usage))
			copy(u[:], i.Usage)
			if _, ok := nodes[i.Name]; !ok {
				node, err := graph.CreateNode(i.Name)
				if err != nil {
					log.Fatal(err)
				}
				nodes[i.Name] = node

				integrations[i.Name] = make(map[data.Direction][]string)
				if _, ok := integrations[i.Name][i.Direction]; !ok {
					integrations[i.Name][i.Direction] = u
				} else {
					integrations[i.Name][i.Direction] = append(integrations[i.Name][i.Direction], u...)
				}
			} else {
				integrations[i.Name][i.Direction] = append(integrations[i.Name][i.Direction], u...)
			}
		}
	}

	for name, node := range nodes {
		for direction, u := range integrations[name] {
			e, err := graph.CreateEdge(name, platformNode, node)
			if err != nil {
				return nil, err
			}
			e.SetLabel(strings.Join(u, "\n"))
			switch direction {
			case data.In:
				e.SetDir(cgraph.BackDir)
			case data.Out:
				e.SetDir(cgraph.ForwardDir)
			case data.Bidirectional:
				e.SetDir(cgraph.BothDir)
			default:
				return nil, fmt.Errorf("unknown direction %v", direction)
			}
		}
	}

	var buf bytes.Buffer
	if err := g.Render(graph, "dot", &buf); err != nil {
		return nil, err
	}

	return DotToImage(diagramName, buf.Bytes())
}
