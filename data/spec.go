package data

import (
	"fmt"
)

type Custom struct {
	XAttributes map[string]interface{}
}

func NewCustom() Custom {
	return Custom{
		XAttributes: make(map[string]interface{}),
	}
}

type BaseSpec struct {
	Name        string
	Description string
	Custom
}

func NewBaseSpec() BaseSpec {
	return BaseSpec{
		Custom: NewCustom(),
	}
}

type OpenHLA struct {
	OpenHla    string
	Info       Info
	Tags       []Tag
	Components []Component
	Layers     []Layer
	Custom
}

func NewOpenHLA() OpenHLA {
	return OpenHLA{
		Info:       NewInfo(),
		Tags:       make([]Tag, 0),
		Layers:     make([]Layer, 0),
		Components: make([]Component, 0),
		Custom:     NewCustom(),
	}
}

type Contact struct {
	Name  string
	Url   string
	Email string
	Custom
}

func NewContact() Contact {
	return Contact{
		Custom: NewCustom(),
	}
}

type ExternalDocs struct {
	Description string
	Url         string
	Custom
}

func NewExternalDocs() ExternalDocs {
	return ExternalDocs{
		Custom: NewCustom(),
	}
}

type Info struct {
	BaseSpec
	TermsOfService string
	Contact        Contact
	License        License
	Version        string
	ExternalDocs   ExternalDocs
}

func NewInfo() Info {
	return Info{
		BaseSpec:     NewBaseSpec(),
		Contact:      NewContact(),
		License:      NewLicense(),
		ExternalDocs: NewExternalDocs(),
	}
}

type License struct {
	Name string
	Url  string
	Custom
}

func NewLicense() License {
	return License{
		Custom: NewCustom(),
	}
}

type Tag struct {
	BaseSpec
}

func NewTag() Tag {
	return Tag{
		BaseSpec: NewBaseSpec(),
	}
}

type Layer struct {
	BaseSpec
}

func NewLayer() Layer {
	return Layer{
		BaseSpec: NewBaseSpec(),
	}
}

type Event struct {
	BaseSpec
	Queue string
}

func NewEvent() Event {
	return Event{
		BaseSpec: NewBaseSpec(),
	}
}

type EventEmitted struct {
	Event
	Payload Payload
}

func NewEventEmitted() EventEmitted {
	return EventEmitted{
		Event:   NewEvent(),
		Payload: NewPayload(),
	}
}

type Payload struct {
	Description string
	ContentType string
	Content     string
	Custom
}

func NewPayload() Payload {
	return Payload{
		Custom: NewCustom(),
	}
}

type OpenApi struct {
	SpecUrl string
	SiteUrl string
	Custom
}

func NewOpenApi() OpenApi {
	return OpenApi{
		Custom: NewCustom(),
	}
}

type Direction int

const (
	Out Direction = iota
	In
	Bidirectional
)

func (e Direction) String() string {
	switch e {
	case Out:
		return "out"
	case In:
		return "in"
	case Bidirectional:
		return "bidirectional"
	default:
		return fmt.Sprintf("%d", int(e))
	}
}

func ParseDirection(s string) (Direction, error) {
	switch s {
	case "in":
		return In, nil
	case "out":
		return Out, nil
	case "bidirectional":
		return Bidirectional, nil
	default:
		return 0, fmt.Errorf("unknown direction %s", s)
	}
}

type Component struct {
	BaseSpec
	Type           string
	OpenApi        OpenApi
	EventsEmitted  []EventEmitted
	EventsConsumed []Event
	DependsOn      []string
	Integrations   []Integration
	Layer          string
	Tags           []string
	Stack          []string
}

func NewComponent() Component {
	return Component{
		BaseSpec:       NewBaseSpec(),
		OpenApi:        NewOpenApi(),
		EventsEmitted:  make([]EventEmitted, 0),
		EventsConsumed: make([]Event, 0),
		DependsOn:      make([]string, 0),
		Integrations:   make([]Integration, 0),
		Tags:           make([]string, 0),
		Stack:          make([]string, 0),
	}
}

type Integration struct {
	BaseSpec
	Direction Direction
	Usage     []string
}

func NewIntegration() Integration {
	return Integration{
		BaseSpec:  NewBaseSpec(),
		Direction: Out,
		Usage:     make([]string, 0),
	}
}
