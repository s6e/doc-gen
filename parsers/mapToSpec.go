package parsers

import (
	"OpenHLADocsGen/data"
	"errors"
	"fmt"
	"strings"
)

const InvalidSpecError = "invalid spec file. Cannot read %s. Expected %s"
const UnsupportedSpecError = "unsupported spec file. Spec version %s. Expected 1.*"
const missingOpenHlaSpecVersionError = "missing openHla spec version"
const FieldIsRequiredError = "%s is required"

func Convert(mapData map[string]interface{}) (*data.OpenHLA, error) {
	return ReadOpenHLA(mapData)
}

const OpenHla = "openhla"
const Info = "info"
const Tags = "tags"
const Tag = "tag"
const Stack = "stack"
const Layers = "layers"
const Layer = "layer"
const Components = "components"
const Component = "component"
const Imports = "imports"
const TermsOfService = "termsOfService"
const Contact = "contact"
const License = "license"
const Version = "version"
const Name = "name"
const Description = "description"
const Url = "url"
const Email = "email"
const Type = "type"
const OpenApi = "open_api"
const EventsEmitted = "events_emitted"
const EventEmitted = "event_emitted"
const EventsConsumed = "events_consumed"
const EventConsumed = "event_consumed"
const Integrations = "integrations"
const Integration = "integration"
const DependsOn = "depends_on"
const Usage = "usage"
const SpecUrl = "spec_url"
const SiteUrl = "site_url"
const Payload = "payload"
const ContentType = "content_type"
const Content = "content"
const Queue = "queue"
const CustomAttributePrefix = "x-"
const ExternalDocs = "externalDocs"
const Direction = "direction"

func throwInvalidSpecError(field string, expected string) error {
	return fmt.Errorf(InvalidSpecError, field, expected)
}

func ReadOpenHLA(mapData map[string]interface{}) (*data.OpenHLA, error) {
	spec := data.NewOpenHLA()
	spec.Custom = ReadCustomAttributes(mapData)

	val, err := ReadString(mapData, OpenHla)
	if err != nil {
		return nil, err
	}

	if val == "" {
		return nil, errors.New(missingOpenHlaSpecVersionError)
	}

	if !strings.HasPrefix(val, "1.") {
		return nil, fmt.Errorf(UnsupportedSpecError, val)
	}

	spec.OpenHla = val

	if val, ok := mapData[Info]; ok {
		if elem, valid := val.(map[string]interface{}); valid {
			infoPtr, err := ReadInfo(elem)
			if err != nil {
				return nil, err
			}
			spec.Info = *infoPtr
		} else {
			return nil, throwInvalidSpecError(Info, "object")
		}
	}

	tags, err := ReadTags(mapData)
	if err != nil {
		return nil, err
	}
	spec.Tags = tags

	layers, err := ReadLayers(mapData)
	if err != nil {
		return nil, err
	}
	spec.Layers = layers

	componentsData, err := GetComponents(mapData)
	if err != nil {
		return nil, err
	}

	components, err := ReadComponents(componentsData)
	if err != nil {
		return nil, err
	}
	spec.Components = components

	return &spec, nil
}

func ReadInfo(mapData map[string]interface{}) (*data.Info, error) {
	info := data.NewInfo()

	baseSpec, err := ReadBase(mapData)
	if err != nil {
		return nil, err
	}
	if baseSpec.Name == "" {
		return nil, fmt.Errorf(FieldIsRequiredError, "info.name")
	}
	info.BaseSpec = *baseSpec

	val, err := ReadString(mapData, TermsOfService)
	if err != nil {
		return nil, err
	}
	info.TermsOfService = val

	if val, ok := mapData[Contact]; ok {
		if elem, valid := val.(map[string]interface{}); valid {
			contact, err := ReadContact(elem)
			if err != nil {
				return nil, err
			}
			info.Contact = *contact
		} else {
			return nil, throwInvalidSpecError(Contact, "object")
		}
	}

	if val, ok := mapData[License]; ok {
		if elem, valid := val.(map[string]interface{}); valid {
			license, err := ReadLicense(elem)
			if err != nil {
				return nil, err
			}
			info.License = *license
		} else {
			return nil, throwInvalidSpecError(License, "object")
		}
	}

	val, err = ReadString(mapData, Version)
	if err != nil {
		return nil, err
	}
	if val == "" {
		return nil, fmt.Errorf(FieldIsRequiredError, "info.version")
	}

	info.Version = val

	if val, ok := mapData[ExternalDocs]; ok {
		if elem, valid := val.(map[string]interface{}); valid {
			externalDocs, err := ReadExternalDocs(elem)
			if err != nil {
				return nil, err
			}
			info.ExternalDocs = *externalDocs
		} else {
			return nil, throwInvalidSpecError(Contact, "object")
		}
	}

	return &info, nil
}

func ReadExternalDocs(mapData map[string]interface{}) (*data.ExternalDocs, error) {
	externalDocs := data.NewExternalDocs()
	externalDocs.Custom = ReadCustomAttributes(mapData)

	val, err := ReadString(mapData, Description)
	if err != nil {
		return nil, err
	}
	externalDocs.Description = val

	val, err = ReadString(mapData, Url)
	if err != nil {
		return nil, err
	}
	if val == "" {
		return nil, fmt.Errorf(FieldIsRequiredError,  "externalDocs.url")
	}
	externalDocs.Url = val

	return &externalDocs, nil
}

func ReadLicense(mapData map[string]interface{}) (*data.License, error) {
	license := data.NewLicense()
	license.Custom = ReadCustomAttributes(mapData)

	val, err := ReadString(mapData, Name)
	if err != nil {
		return nil, err
	}
	if val == "" {
		return nil, fmt.Errorf(FieldIsRequiredError,  "license.name")
	}
	license.Name = val

	val, err = ReadString(mapData, Url)
	if err != nil {
		return nil, err
	}
	license.Url = val

	return &license, nil
}

func ReadContact(mapData map[string]interface{}) (*data.Contact, error) {
	contact := data.NewContact()
	contact.Custom = ReadCustomAttributes(mapData)

	val, err := ReadString(mapData, Name)
	if err != nil {
		return nil, err
	}
	contact.Name = val

	val, err = ReadString(mapData, Url)
	if err != nil {
		return nil, err
	}
	contact.Url = val

	val, err = ReadString(mapData, Email)
	if err != nil {
		return nil, err
	}
	contact.Email = val

	return &contact, nil
}

func ReadTags(mapData map[string]interface{}) ([]data.Tag, error) {
	if v, ok := mapData[Tags]; ok {
		if t, valid := v.([]interface{}); valid {
			tags := make([]data.Tag, len(t))
			for index, val := range t {
				if elem, valid := val.(map[string]interface{}); valid {
					tag, err := ReadTag(elem)
					if err != nil {
						return nil, err
					}
					tags[index] = *tag
				} else {
					return nil, throwInvalidSpecError(Tag, "object")
				}
			}
			return tags, nil
		} else {
			return nil, throwInvalidSpecError(Tags, "array")
		}
	}
	return make([]data.Tag, 0), nil
}

func ReadTag(mapData map[string]interface{}) (*data.Tag, error) {
	tag := data.NewTag()

	baseSpec, err := ReadBase(mapData)
	if err != nil {
		return nil, err
	}
	tag.BaseSpec = *baseSpec

	return &tag, nil
}

func ReadLayers(mapData map[string]interface{}) ([]data.Layer, error) {
	if vals, ok := mapData[Layers]; ok {
		if t, valid := vals.([]interface{}); valid {
			layers := make([]data.Layer, len(t))
			for index, val := range t {
				if elem, valid := val.(map[string]interface{}); valid {
					layer, err := ReadLayer(elem)
					if err != nil {
						return nil, err
					}
					layers[index] = *layer
				} else {
					return nil, throwInvalidSpecError(Layer, "object")
				}
			}
			return layers, nil
		} else {
			return nil, throwInvalidSpecError(Layers, "array")
		}
	}
	return make([]data.Layer, 0), nil
}

func ReadLayer(mapData map[string]interface{}) (*data.Layer, error) {
	layer := data.NewLayer()

	baseSpec, err := ReadBase(mapData)
	if err != nil {
		return nil, err
	}
	layer.BaseSpec = *baseSpec

	return &layer, nil
}

func GetComponents(mapData map[string]interface{}) ([]interface{}, error) {
	if val, ok := mapData[Components]; ok {
		if elem, valid := val.([]interface{}); valid {
			return elem, nil
		} else {
			return nil, throwInvalidSpecError(Components, "array")
		}
	}

	return make([]interface{}, 0), nil
}

func ReadComponents(mapData []interface{}) ([]data.Component, error) {
	components := make([]data.Component, len(mapData))
	for index, val := range mapData {
		if elem, valid := val.(map[string]interface{}); valid {
			component, err := ReadComponent(elem)
			if err != nil {
				return nil, err
			}
			components[index] = *component
		} else {
			return nil, throwInvalidSpecError(Component, "object")
		}
	}
	return components, nil
}

func ReadComponent(mapData map[string]interface{}) (*data.Component, error) {
	component := data.NewComponent()

	baseSpec, err := ReadBase(mapData)
	if err != nil {
		return nil, err
	}
	if baseSpec.Name == "" {
		return nil, fmt.Errorf(FieldIsRequiredError,  "component.name")
	}
	component.BaseSpec = *baseSpec

	val, err := ReadString(mapData, Type)
	if err != nil {
		return nil, err
	}
	component.Type = val

	if val, ok := mapData[OpenApi]; ok {
		if elem, valid := val.(map[string]interface{}); valid {
			openApi, err := ReadOpenApi(elem)
			if err != nil {
				return nil, err
			}
			component.OpenApi = *openApi
		} else {
			return nil, throwInvalidSpecError(OpenApi, "object")
		}
	}

	if val, ok := mapData[EventsEmitted]; ok {
		if elem, valid := val.([]interface{}); valid {
			eventsEmitted, err := ReadEventsEmitted(elem)
			if err != nil {
				return nil, err
			}
			component.EventsEmitted = eventsEmitted
		} else {
			return nil, throwInvalidSpecError(EventsEmitted, "array")
		}
	}

	if val, ok := mapData[EventsConsumed]; ok {
		if elem, valid := val.([]interface{}); valid {
			eventsConsumed, err := ReadEventsConsumed(elem)
			if err != nil {
				return nil, err
			}
			component.EventsConsumed = eventsConsumed
		} else {
			return nil, throwInvalidSpecError(EventsConsumed, "array")
		}
	}

	if val, ok := mapData[Integrations]; ok {
		if elem, valid := val.([]interface{}); valid {
			integrations, err := ReadIntegrations(elem)
			if err != nil {
				return nil, err
			}
			component.Integrations = integrations
		} else {
			return nil, throwInvalidSpecError(Integrations, "array")
		}
	}

	val, err = ReadString(mapData, Layer)
	if err != nil {
		return nil, err
	}
	component.Layer = val

	tags, err := ReadStringArray(mapData, Tags)
	if err != nil {
		return nil, err
	}
	component.Tags = tags

	stack, err := ReadStringArray(mapData, Stack)
	if err != nil {
		return nil, err
	}
	component.Stack = stack

	dependsOn, err := ReadStringArray(mapData, DependsOn)
	if err != nil {
		return nil, err
	}
	component.DependsOn = dependsOn

	return &component, nil
}

func ReadIntegrations(mapData []interface{}) ([]data.Integration, error) {
	integrations := make([]data.Integration, len(mapData))
	for i, val := range mapData {
		if elem, valid := val.(map[string]interface{}); valid {
			integration, err := ReadIntegration(elem)
			if err != nil {
				return nil, err
			}
			integrations[i] = *integration
		} else {
			return nil, throwInvalidSpecError(Integration, "object")
		}
	}
	return integrations, nil
}

func ReadIntegration(mapData map[string]interface{}) (*data.Integration, error) {
	integration := data.NewIntegration()

	baseSpec, err := ReadBase(mapData)
	if err != nil {
		return nil, err
	}
	integration.BaseSpec = *baseSpec

	usage, err := ReadStringArray(mapData, Usage)
	if err != nil {
		return nil, err
	}
	integration.Usage = usage

	if val, ok := mapData[Direction]; ok {
		if d, valid := val.(string); valid {
			direction, err := data.ParseDirection(d)
			if err != nil {
				return nil, err
			}
			integration.Direction = direction
		} else {
			return nil, throwInvalidSpecError(Direction, "string")
		}
	}

	return &integration, nil
}

func ReadOpenApi(mapData map[string]interface{}) (*data.OpenApi, error) {
	openApi := data.NewOpenApi()
	openApi.Custom = ReadCustomAttributes(mapData)

	val, err := ReadString(mapData, SpecUrl)
	if err != nil {
		return nil, err
	}
	if val == "" {
		return nil, fmt.Errorf(FieldIsRequiredError,  "openApi.url")
	}
	openApi.SpecUrl = val

	val, err = ReadString(mapData, SiteUrl)
	if err != nil {
		return nil, err
	}
	openApi.SiteUrl = val

	return &openApi, nil
}

func ReadEventsConsumed(mapData []interface{}) ([]data.Event, error) {
	events := make([]data.Event, len(mapData))
	for i, val := range mapData {
		if elem, valid := val.(map[string]interface{}); valid {
			event, err := ReadEvent(elem)
			if err != nil {
				return nil, err
			}
			events[i] = *event
		} else {
			return nil, throwInvalidSpecError(EventConsumed, "object")
		}
	}
	return events, nil
}

func ReadEventsEmitted(mapData []interface{}) ([]data.EventEmitted, error) {
	events := make([]data.EventEmitted, len(mapData))
	for i, e := range mapData {
		if elem, valid := e.(map[string]interface{}); valid {
			event, err := ReadEventEmitted(elem)
			if err != nil {
				return nil, err
			}
			events[i] = *event
		} else {
			return nil, throwInvalidSpecError(EventEmitted, "object")
		}
	}
	return events, nil
}

func ReadEventEmitted(mapData map[string]interface{}) (*data.EventEmitted, error) {
	event := data.NewEventEmitted()
	e, err := ReadEvent(mapData)
	if err != nil {
		return nil, err
	}
	event.Event = *e

	if val, ok := mapData[Payload]; ok {
		if elem, valid := val.(map[string]interface{}); valid {
			payload, err := ReadPayload(elem)
			if err != nil {
				return nil, err
			}
			event.Payload = *payload
		} else {
			return nil, throwInvalidSpecError(Payload, "object")
		}
	}

	return &event, nil
}

func ReadPayload(mapData map[string]interface{}) (*data.Payload, error) {
	payload := data.NewPayload()
	payload.Custom = ReadCustomAttributes(mapData)

	val, err := ReadString(mapData, Description)
	if err != nil {
		return nil, err
	}
	payload.Description = val

	val, err = ReadString(mapData, ContentType)
	if err != nil {
		return nil, err
	}
	if val == "" {
		return nil, fmt.Errorf(FieldIsRequiredError,  "payload.contentType")
	}
	payload.ContentType = val

	val, err = ReadString(mapData, Content)
	if err != nil {
		return nil, err
	}
	payload.Content = val

	return &payload, nil
}

func ReadEvent(mapData map[string]interface{}) (*data.Event, error) {
	event := data.NewEvent()

	baseSpec, err := ReadBase(mapData)
	if err != nil {
		return nil, err
	}
	if baseSpec.Name == "" {
		return nil, fmt.Errorf(FieldIsRequiredError,  "event.name")
	}
	event.BaseSpec = *baseSpec

	val, err := ReadString(mapData, Queue)
	if err != nil {
		return nil, err
	}
	event.Queue = val

	return &event, nil
}

func ReadCustomAttributes(mapData map[string]interface{}) data.Custom {
	custom := data.NewCustom()
	for key, element := range mapData {
		if strings.HasPrefix(key, CustomAttributePrefix) {
			custom.XAttributes[key] = element
		}
	}

	return custom
}

func ReadBase(mapData map[string]interface{}) (*data.BaseSpec, error) {
	spec := data.NewBaseSpec()
	spec.Custom = ReadCustomAttributes(mapData)

	val, err := ReadString(mapData, Name)
	if err != nil {
		return nil, err
	}
	spec.Name = val

	val, err = ReadString(mapData, Description)
	if err != nil {
		return nil, err
	}
	spec.Description = val

	return &spec, nil
}

func ReadString(mapData map[string]interface{}, field string) (string, error) {
	if val, ok := mapData[field]; ok {
		if desc, valid := val.(string); valid {
			return desc, nil
		}
		return "", throwInvalidSpecError(field, "string")
	}

	return "", nil
}

func ReadStringArray(mapData map[string]interface{}, field string) ([]string, error) {
	if val, ok := mapData[field]; ok {
		if slice, valid := val.([]interface{}); valid {
			list := make([]string, len(slice))
			for i, t := range slice {
				if s, ok := t.(string); ok {
					list[i] = s
				} else {
					return nil, throwInvalidSpecError(field, "string")
				}
			}

			return list, nil
		}
		return nil, throwInvalidSpecError(field, "array")
	}

	return make([]string, 0), nil
}
