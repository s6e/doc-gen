package parsers

import (
	"OpenHLADocsGen/data"
	"OpenHLADocsGen/parsers/loaders"
	"errors"
	"fmt"
	"gopkg.in/yaml.v3"
	"path/filepath"
)

const missingSupportedExtensionsError = "missing supported extensions"

type Parser interface {
	Parse(spec string) (*data.OpenHLA, error)
}

type ParserRegistration struct {
	UnmarshalFunc       func(in []byte, out interface{}) (err error)
	SupportedExtensions []string
}

var parsers = make(map[string]ParserRegistration)

func Register(name string, unmarshalFunc func(in []byte, out interface{}) (err error), supportedExtensions []string) error {
	if len(supportedExtensions) == 0 {
		return errors.New(missingSupportedExtensionsError)
	}

	parsers[name] = ParserRegistration{
		unmarshalFunc,
		supportedExtensions,
	}
	return nil
}

func GetByExtension(ext string) (func(in []byte, out interface{}) (err error), error) {
	for _, reg := range parsers {
		for _, e := range reg.SupportedExtensions {
			if e == ext {
				return reg.UnmarshalFunc, nil
			}
		}
	}

	return nil, fmt.Errorf("parser for extension %s doesn't exists", ext)
}

func List() []string {
	keys := make([]string, len(parsers))

	i := 0
	for exporterName := range parsers {
		keys[i] = exporterName
		i++
	}

	return keys
}

func RegisterBase() error {
	err := Register(
		"json",
		yaml.Unmarshal,
		[]string{".json"},
	)
	if err != nil {
		return err
	}

	err = Register(
		"yaml",
		yaml.Unmarshal,
		[]string{".yaml", ".yml"},
	)
	if err != nil {
		return err
	}

	return nil
}

func Parse(specFilePath string) (*data.OpenHLA, error) {
	unmarshal, err := GetByExtension(filepath.Ext(specFilePath))
	if err != nil {
		return nil, err
	}

	specFile, err := loaders.Load(specFilePath)
	if err != nil {
		return nil, err
	}

	specData := make(map[string]interface{})
	err = unmarshal(specFile, &specData)
	if err != nil {
		return nil, err
	}

	openHla, err := Convert(specData)
	if err != nil {
		return nil, err
	}

	imports, err := ReadImports(specFilePath, specData)
	if err != nil {
		return nil, err
	}

	AddImportSpecs(imports, openHla)

	return openHla, nil
}

func AddImportSpecs(imports []data.OpenHLA, openHla *data.OpenHLA) {
	for _, importSpec := range imports {
		AddSpec(openHla, importSpec)
	}
}

func AddSpec(openHla *data.OpenHLA, importSpec data.OpenHLA) {
	openHla.Components = append(openHla.Components, importSpec.Components...)
	openHla.Tags = append(openHla.Tags, importSpec.Tags...)
	//openHla.XAttributes = append(openHla.XAttributes, importSpec.XAttributes...)
}

func ReadImports(currentSpecPath string, specData map[string]interface{}) ([]data.OpenHLA, error) {
	if imports, ok := specData[Imports]; ok {
		if val, valid := imports.([]interface{}); valid {
			importSpecs := make([]data.OpenHLA, len(val))
			for i, importPath := range val {
				if importPathString, isString := importPath.(string); isString {
					importSpec, err := Parse(loaders.ResolveImportPath(currentSpecPath, importPathString))
					if err != nil {
						return nil, err
					}

					importSpecs[i] = *importSpec
				} else {
					return nil, errors.New(InvalidSpecError)
				}
			}
			return importSpecs, nil
		} else {
			return nil, errors.New(InvalidSpecError)
		}
	}

	return make([]data.OpenHLA, 0), nil
}
