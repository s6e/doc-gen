package loaders

import (
	"bytes"
	scp "github.com/bramvdbogaerde/go-scp"
	"github.com/bramvdbogaerde/go-scp/auth"
	"golang.org/x/crypto/ssh"
	"net/url"
	"os/user"
	"path/filepath"
)

type SshLoader struct {
}

func (SshLoader) load(filePath string) ([]byte, error) {
	currentUser, err := user.Current()
	if err != nil {
		return nil, err
	}

	fileUrl, err := url.Parse(filePath)
	if err != nil {
		return nil, err
	}

	var userName string
	if fileUrl.User != nil && fileUrl.User.Username() != "" {
		userName = fileUrl.User.Username()
	} else {
		userName = currentUser.Name
	}

	clientConfig, _ := auth.PrivateKey(userName, filepath.Join(currentUser.HomeDir, ".ssh/id_rsa"), ssh.InsecureIgnoreHostKey())

	host := fileUrl.Host
	if fileUrl.Port() == "" {
		host += ":22"
	}
	client := scp.NewClient(host, &clientConfig)
	err = client.Connect()
	if err != nil {
		return nil, err
	}
	defer client.Close()

	var b bytes.Buffer = bytes.Buffer{}
	if err = client.CopyFromRemotePassThru(&b, fileUrl.Path, nil); err != nil {
		return nil, err
	}

	return b.Bytes(), nil
}

func (SshLoader) applies(filePath string) bool {
	u, err := url.Parse(filePath)
	if err != nil {
		return false
	}
	return u.Scheme == "ssh" || u.Scheme == "scp"
}

func (SshLoader) resolveImportPath(parentPath string, importPath string) string {
	baseUrl, _ := url.Parse(parentPath)
	importUrl, _ := url.Parse(importPath)

	return baseUrl.ResolveReference(importUrl).String()
}
