package loaders

import (
	"io/ioutil"
	"path/filepath"
)

type LocalLoader struct {
}

func (LocalLoader) load(filePath string) ([]byte, error) {
	return ioutil.ReadFile(filePath)
}

func (LocalLoader) applies(filePath string) bool {
	return true
}

func (LocalLoader) resolveImportPath(parentPath string, importPath string) string {
	if filepath.IsAbs(importPath) {
		return filepath.Clean(importPath)
	}

	return filepath.Join(filepath.Dir(parentPath), filepath.Clean(importPath))
}
