package loaders

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

type WebLoader struct {
}

func (WebLoader) load(filePath string) ([]byte, error) {
	client := http.Client{}

	resp, _ := client.Get(filePath)
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(resp.Body)
	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		return bodyBytes, nil
	}

	return nil, fmt.Errorf("error fetching file. StatusCode: %v", resp.StatusCode)
}

func (WebLoader) applies(filePath string) bool {
	u, err := url.Parse(filePath)
	if err != nil {
		return false
	}
	return u.Scheme == "http" || u.Scheme == "https"
}

func (WebLoader) resolveImportPath(parentPath string, importPath string) string {
	baseUrl, _ := url.Parse(parentPath)
	importUrl, _ := url.Parse(importPath)

	return baseUrl.ResolveReference(importUrl).String()
}
