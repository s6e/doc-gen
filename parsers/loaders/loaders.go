package loaders

type Loader interface {
	load(filePath string) ([]byte, error)
	applies(filePath string) bool
	resolveImportPath(parentPath string, importPath string) string
}

var loaders = make([]Loader, 0)

func Register(loader Loader) {
	loaders = append(loaders, loader)
}

func getLoader(path string) Loader {
	for _, loader := range loaders {
		if loader.applies(path) {
			return loader
		}
	}

	return LocalLoader{}
}

func Load(path string) ([]byte, error) {
	return getLoader(path).load(path)
}

func ResolveImportPath(parentPath string, importPath string) string {
	if isSpecial(importPath) {
		return importPath
	}
	return getLoader(parentPath).resolveImportPath(parentPath, importPath)
}

func isSpecial(path string) bool {
	for _, loader := range loaders {
		if loader.applies(path) {
			return true
		}
	}

	return false
}
