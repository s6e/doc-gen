package main

import (
	"OpenHLADocsGen/exporters"
	"OpenHLADocsGen/parsers"
	"OpenHLADocsGen/parsers/loaders"
	"fmt"
	"github.com/akamensky/argparse"
	"os"
	"strings"
)

type Options struct {
	Spec         string
	Output       string
	OutputFormat string
}

func parseArgs(registeredExporters string) (Options, error) {
	parser := argparse.NewParser("print", "Prints provided string to stdout")
	spec := parser.String("s", "spec", &argparse.Options{Required: false, Help: "OpenHLA specification file", Default: "openhla.json"})
	outputDirPtr := parser.String("d", "output-dir", &argparse.Options{Required: false, Help: "Output directory", Default: "build"})
	outputFormatPtr := parser.String("o", "output",
		&argparse.Options{Required: false, Help: fmt.Sprintf("Output document format. Allowed values: %s", registeredExporters), Default: "html"})

	err := parser.Parse(os.Args)
	if err != nil {
		fmt.Print(parser.Usage(err))
		return Options{}, err
	}

	return Options{
		Spec:         *spec,
		Output:       *outputDirPtr,
		OutputFormat: *outputFormatPtr,
	}, err
}

func main() {
	code := 0
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("Recovering from panic: %v \n", r)
			os.Exit(99)
		}

		os.Exit(code)
	}()

	err := parsers.RegisterBase()
	if err != nil {
		fmt.Println(err)
		code = 1
		return
	}

	err = exporters.RegisterBase()
	if err != nil {
		fmt.Println(err)
		code = 2
		return
	}

	loaders.Register(loaders.WebLoader{})
	loaders.Register(loaders.SshLoader{})

	//todo: plugins loading here

	registeredExporters := strings.Join(exporters.List(), ", ")

	opts, err := parseArgs(registeredExporters)
	if err != nil {
		fmt.Println(err)
		code = 3
		return
	}

	exporter, err := exporters.Get(opts.OutputFormat)
	if err != nil {
		fmt.Println(err)
		code = 4
		return
	}

	specPtr, err := parsers.Parse(opts.Spec)
	if err != nil {
		fmt.Println(err)
		code = 5
		return
	}

	err = exporter.Export(*specPtr, opts.Output)
	if err != nil {
		fmt.Println(err)
		code = 6
		return
	}
}
