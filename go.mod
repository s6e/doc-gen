module OpenHLADocsGen

go 1.17

require (
	github.com/akamensky/argparse v1.3.1 // indirect
	github.com/awalterschulze/gographviz v0.0.0-20200901124122-0eecad45bd71 // indirect
	github.com/blushft/go-diagrams v0.0.0-20201006005127-c78c821223d9 // indirect
	github.com/bramvdbogaerde/go-scp v1.1.0 // indirect
	github.com/fogleman/gg v1.3.0 // indirect
	github.com/goccy/go-graphviz v0.0.9 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/gomarkdown/markdown v0.0.0-20210918233619-6c1113f12c4a // indirect
	github.com/google/uuid v1.1.2 // indirect
	github.com/iancoleman/strcase v0.2.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rakyll/statik v0.1.7 // indirect
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1 // indirect
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	golang.org/x/sys v0.0.0-20210525143221-35b2ab0089ea // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
